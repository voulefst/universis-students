import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import { ConfigurationService, ModalService, ErrorService } from '@universis/common';
import {TranslateModule} from '@ngx-translate/core';
import {ModalModule} from 'ngx-bootstrap';
import {ToastrModule} from 'ngx-toastr';
import {SharedModule} from '@universis/common';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {LoadingService} from '@universis/common';
import {RequestsListComponent} from './requests-list.component';
import {TestingConfigurationService} from '../../../test';


describe('RequestsListComponent', () => {
    let component: RequestsListComponent;
    let fixture: ComponentFixture<RequestsListComponent>;

    const modalSvc = jasmine.createSpyObj('ModalService' , ['showDialog', 'openModal']);
    const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading', 'hideLoading']);
    const errorSvc = jasmine.createSpyObj('ErrorService', ['navigateToError']);

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [
                RequestsListComponent
            ],
            imports: [
                FormsModule,
                InfiniteScrollModule,
                RouterTestingModule,
                HttpClientTestingModule,
                ModalModule.forRoot(),
                ToastrModule.forRoot(),
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                SharedModule
            ],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                },
                {
                    provide: ModalService,
                    useValue: modalSvc
                },
              {
                provide: LoadingService,
                useValue: loadingSvc
              },
              {
                provide: ErrorService,
                useValue: errorSvc
              }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RequestsListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
