import {inject, TestBed} from '@angular/core/testing';
import { MessagesService } from './messages.service';
import {AngularDataContext, MostModule} from '@themost/angular';
import {ConfigurationService} from '@universis/common';
import {TestingConfigurationService} from '@universis/common/testing';

describe('MessagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      MostModule.forRoot({
        base: '/api/',
        options: {
          useMediaTypeExtensions: false
        }
      })
    ],
    providers: [
      {
        provide: ConfigurationService,
        useClass: TestingConfigurationService
      }
    ]
  }));

  it('should be created', inject([AngularDataContext], (context: AngularDataContext, configurationService: ConfigurationService) => {
    const service = new MessagesService(context, configurationService);
    expect(service).toBeTruthy();
  }));
});
