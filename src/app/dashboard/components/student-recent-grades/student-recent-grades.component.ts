import { Component, OnInit } from '@angular/core';
import {GradesService} from '../../../grades/services/grades.service';

@Component({
  selector: 'app-student-recent-grades',
  templateUrl: './student-recent-grades.component.html',
  styleUrls: ['./student-recent-grades.component.scss']
})
export class StudentRecentGradesComponent implements OnInit {

  public latestCourses: any = [];
  public isLoading = true;   // Only if data is loaded

  constructor(private gradesService: GradesService) { }

  ngOnInit() {
    this.gradesService.getRecentGrades().then((res) => {
      this.latestCourses = res;
      if (Array.isArray(this.latestCourses)) {
        this.latestCourses.sort((a, b) => a.course.name.localeCompare(b.course.name));
      }
      this.isLoading = false; // Data is loaded
    });

  }

}
